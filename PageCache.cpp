#include"PageCache.h"

PageCache PageCache::_sInst;

//提取对印映射位置的span页，若该位为空，则一直向后寻找 
Span* PageCache::NewSpan(size_t k)
{
	assert(k > 0);//k作为pagecache层桶的直接映射下标，必须在：0<k<129的范围内才有效
	
	if (k > NPAGES - 1)//如果k大于128页(如果线程要申请的对象大于page层的128页，那么直接向堆申请）
	{
		void* ptr = SystemAlloc(k);//直接向堆申请一块k大小的空间

		Span* span = _spanPool.New();
		span->_pageId = (PAGE_ID)ptr >> PAGE_SHIFT;//记录这一块空间的起始页号
		span->_n = k;//记录页数大小

		//_idSpanMap[span->_pageId] = span;//记录映射关系
		_idSpanMap.set(span->_pageId, span);

		return span;
	}

	//若该pagecache层的 K 桶位有页资源
	if (!_spanLists[k].Empty())
	{
		Span* kSpan = _spanLists[k].PopFront();//返回一个k桶位的span节点

		//记录对应的页号与span指针的映射关系,方便centarl cache回收小块内存时，查找到对应的span节点
		for (PAGE_ID i = 0; i < kSpan->_n; i++)
		{
			//_idSpanMap[kSpan->_pageId + i] = kSpan;//记录被拿出去span的映射关系，方便后期回收
			_idSpanMap.set(kSpan->_pageId + i, kSpan);
		}

		return kSpan;
	}

	//若走到这，则说明第K桶位没有页资源,便继续向K位之后寻找页资源，如果有可以把该页资源进行切分再调整
	for (size_t i = k + 1; i < NPAGES; i++)
	{
		if (!_spanLists[i].Empty())//若找到不为空的桶位，则直接将桶内资源进行切割
		{
			//如何切割：
			//1、将_spanLists[i]的资源切分成：（K大小的span） 与 （N-K大小的span）
			//2、K大小的span返回，N-K大小的span挂接回对应大小的桶位中
			Span* nSpan = _spanLists[i].PopFront();//取出一个span页节点，此时的nspan指向整个页节点
			Span* kSpan = _spanPool.New();//创建一个span

			//头切
			//切割出一个K大小的页
			kSpan->_pageId = nSpan->_pageId;//kspan页号等于nspan
			kSpan->_n = k;//kspan的页数等于k，表示切分了k个页（可使用）

			//剩余的N-K页
			nSpan->_pageId += k;//nspan的页号移动k个
			nSpan->_n -= k;//页数-k个（_n意义:页数、桶位下标、页大小）

			//将N-K span插入到对应位的桶位中
			_spanLists[nSpan->_n].PushFront(nSpan);
			//存储nspan的首尾页号和nspan的映射关系，方便page cache回收前查找
			//_idSpanMap[nSpan->_pageId] = nSpan;
			//_idSpanMap[nSpan->_pageId + nSpan->_n - 1] = nSpan;
			_idSpanMap.set(nSpan->_pageId, nSpan);
			_idSpanMap.set(nSpan->_pageId + nSpan->_n - 1, nSpan);

			//记录对应的页号与span指针的映射关系,方便centarl cache回收小块内存时，查找到对应的span节点
			for (PAGE_ID i = 0; i < kSpan->_n; i++)
			{
				//_idSpanMap[kSpan->_pageId + i] = kSpan;//记录被拿出去span的映射关系，方便后期回收
				_idSpanMap.set(kSpan->_pageId + i, kSpan);
			}

			return kSpan;//返回kspan页
		}
	}

	//若走到这个位置，则说明_spanlists桶内没有可满足K>大小的页资源
	//则需要向 堆 申请空间一块大小为 “128kb”的页
	Span* bigSpan = _spanPool.New();
	void* ptr = SystemAlloc(NPAGES - 1);//申请128个大小的页
	bigSpan->_pageId = (PAGE_ID)ptr >> PAGE_SHIFT;//求页号：起始位置 / 2^13 正确
	bigSpan->_n = NPAGES - 1;//求页数：128

	//切分
	_spanLists[bigSpan->_n].PushFront(bigSpan);//将申请下来的128K页插入到对应的桶位下
	return NewSpan(k);//重新调用自己，进行分配与分割工作
}


//获取对象到span的映射(返回span的地址)
Span* PageCache::MapObjectSpan(void* obj)
{
	PAGE_ID id = ((PAGE_ID)obj >> PAGE_SHIFT);//用地址算出对象的页号 /8^1024
	
	//std::unique_lock<std::mutex> lock(_pageMtx);//加锁
	//auto ret = _idSpanMap.find(id);//通过页号映射找到span节点的地址
	//if (ret != _idSpanMap.end())//找到了对应的span节点
	//{
	//	return ret->second;//返回span指针
	//}
	//else
	//{
	//	assert(false);//每次中心层给到线程层前都会有记录映射关系 不存在找不到
	//	return nullptr;
	//}

	auto ret = (Span*)_idSpanMap.get(id);
	assert(ret != nullptr);
	return ret;
}

//释放span节点回到page cache，合并内部相邻的span节点成为更大的页(解决外部碎片的问题）
void PageCache::ReleaseSpanToPageCache(Span* span)
{
	if (span->_n > NPAGES - 1)//如果该span的页数大于128页
	{
		void* ptr = (void*)(span->_pageId << PAGE_SHIFT);//取到span的地址
		SystemFree(ptr);//释放那块由堆空间直接申请的空间（必然大于128页，page层无法维护）
		_spanPool.Delete(span);
		return;
	}

	//对span前后的相邻页，尝试进行合并，缓解外部碎片问题  例如：...99-span页号：100-101...
	//遇到空停止搜寻，若非空，则一直搜寻
	//若搜寻到的span没有被分配出去使用，则可以合并，若是正在被使用，则不可以合并
	//若合并的页数超过了128，则停止合并

	while (true)//向前合并
	{
		PAGE_ID prevId = span->_pageId - 1;//查看前一个span是否在
		//auto ret = _idSpanMap.find(prevId);
		//if (ret == _idSpanMap.end())//若不存在y页
		//{
		//	break;//不再向前搜寻合并
		//}
		auto ret = (Span*)_idSpanMap.get(prevId);
		if (ret == nullptr)
		{
			break;
		}

		Span* prevSpan = ret;
		if (prevSpan->_isUse == true)//如果存在页，但是该span被使用着
		{
			break;//不再向前搜寻合并
		}

		if (prevSpan->_n + span->_n > NPAGES - 1)//若 span的页数 + span前一页号的页数 > 128
		{
			break;//不再向前搜寻合并
		}

		//若走到这里，便是可以合并的
		span->_pageId = prevSpan->_pageId;//页号等于前一位的页号
		span->_n += prevSpan->_n;//页数 += 前一位的页数 (便完成了合并动作)

		_spanLists[prevSpan->_n].Erase(prevSpan);//删除该span在对应桶位的存在
		_spanPool.Delete(prevSpan);//释放span前一位页的空间
	}

	while (true)//向后合并
	{
		PAGE_ID nextId = span->_pageId + span->_n;//span的下一个页号 (+_n是因为要越过自身页数，找到真正的下一个页号）
		//auto ret = _idSpanMap.find(nextId);
		//if (ret == _idSpanMap.end())//如果后一个页号没有span页资源
		//{
		//	break;//不再向后搜寻合并
		//}
		auto ret = (Span*)_idSpanMap.get(nextId);
		if (ret == nullptr)
		{
			break;
		}

		Span* nextSpan = ret;
		if (nextSpan->_isUse == true)//如果存在页，但是该span被使用着
		{
			break;//不再向后搜寻合并
		}

		if (nextSpan->_n + span->_n > NPAGES - 1)//若 span的页数 + span后一页号的页数 > 128
		{
			break;//不再向后搜寻合并
		}

		//合并逻辑
		span->_n += nextSpan->_n;//span += 后一页的页数
		_spanLists[nextSpan->_n].Erase(nextSpan);//删除该span在对应桶位的存在，因为已经被合并了
		_spanPool.Delete(nextSpan);
	}

	//将合并后的span挂到对应页数的桶位（或者没有发生合并的单一span）
	_spanLists[span->_n].PushFront(span);//头插进对应桶位

	//构造让别人可以合并“我”的条件
	span->_isUse = false;//当且span处于没有被使用状态
	//_idSpanMap[span->_pageId] = span;//记录合并后资源的 首页号
	//_idSpanMap[span->_pageId + span->_n - 1] = span;//记录合并后资源的 尾页号
	_idSpanMap.set(span->_pageId, span);
	_idSpanMap.set(span->_pageId + span->_n - 1, span);
}