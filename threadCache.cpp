#include"threadCache.h"
#include"CentralCache.h"

//线程向中心内存获取内存对象（多线程访问场景）需要加锁保护
void* ThreadCache::FetchFromCentralCache(size_t index, size_t size)
{
	//慢开始反馈调节算法（批量申请） 
	// [2,512]
	//作用：1、第一次向中心缓存申请空间时不会申请过多的批量空间
	//		2、若发生多次申请size大的情况，则该桶位的上限值自增，直到512的最终上限（缓慢自增）
	//		3、size越小，一次分配给thread cache的空间越多
	//		4、size越大，一次分配给thread cache的空间越少
	//批量获取是为了保证下一次thread cache可以在自己的缓存内获取，而不用跑到中心缓存内加锁获取资源
	size_t batchNum = min(_freeLists[index].MaxSize(),SizeClass::NumMoveSize(size));//NumMoveSize(size):计算该对象将申请空间的个数（size * NumMoveSize == N个） batchNum是一个最终的批量值
	if (_freeLists[index].MaxSize() == batchNum)//若该桶位的上限值与batchnum相等，则该桶位上限值++
	{
		_freeLists[index].MaxSize() += 1;//2、3、4、5......（_maxSize）可调节
	}
	
	void* start = nullptr;//资源的开始位
	void* end = nullptr;//资源的结尾位
	size_t actualNum = CentralCache::GetInstance()->FetchRangeObj(start, end, batchNum, size);//内存块的开始 内存块的结尾 申请多少个 对象的大小 (作用：取出一块批量内存)
	assert(actualNum > 0);//最终取到的内存块数必须大于0
	if (actualNum == 1)//若返回的有效内存块仅为1个
	{
		assert(start == end);//则start==end
		return start;//返回任意一个
	}
	else//若申请到了多个内存块，则需要将多个内存块，头插进对应桶位的自由链表内
	{
		_freeLists[index].PushRange(NextObj(start), end, actualNum-1);//将多个内存块的第二个到结尾的内存块连接进对应桶的自由链表（因为第一块将被返回给threadcache使用）
		return start;//返回第一个内存块（只能是一块，因为对象大小早就被对齐）
	}
}

//申请内存对象
void* ThreadCache::Allocate(size_t size)//参数size为申请空间的大小
{
	assert(size <= MAX_BYTES);//申请对象的大小不大于256KB
	size_t alignSize = SizeClass::RoundUp(size);//算出对齐后的对象大小（由对齐数给出最后的内存块大小）
	size_t index = SizeClass::Index(size);//对象经过映射规则后得出的桶下标

	if (!_freeLists[index].Empty())//若对应桶下标的自由链表不为空，则返回一个内存块资源 (此处为无锁获取资源)
	{
		return _freeLists[index].Pop();//发配一个对应下标下自由链表上的一个内存块
	}
	else//若对应桶下标的自由链表为空（没有资源），则向下一层“中心缓存”申请空间
	{
		return FetchFromCentralCache(index,alignSize);//向中心缓存层申请一个：在对应哈希桶处，创建一个对齐后的内存块资源（头插）
	}
}

//释放内存对象
void ThreadCache::Deallocate(void* ptr, size_t size)//size同理(对象大小)
{
	assert(ptr);//被释放的资源不能为空
	assert(size <= MAX_BYTES);//释放对象的大小不大于256KB
	size_t index = SizeClass::Index(size);//通过对象的大小去计算对应的桶下标
	_freeLists[index].Push(ptr);//将待释放的内存块头插进“对应桶下标的自由链表内”（threadcache层的小内存块哈希桶） 

	//若当前桶内的内存块长度 大于等于 内存块申请上限（批次）
	if (_freeLists[index].Size() >= _freeLists[index].MaxSize())
	{
		ListTooLong(_freeLists[index], size);//则把这一块（批次）内存块回收到span中
	}
}

//当链表节点数超过了上限后，将该批量内存块回收到中心缓存（作用：获取到即将释放的批次内存范围）
void ThreadCache::ListTooLong(FreeList& list, size_t size)
{
	void* start = nullptr;
	void* end = nullptr;
	list.PopRange(start, end, list.MaxSize());//取出一块批量内存

	CentralCache::GetInstance()->ReleaseListToSpans(start, size);//将这一块批量内存还给中心缓存的span中
}