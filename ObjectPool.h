#pragma once
#include"common.h"

template<class T>
class ObjectPool//内存池类
{
public:

	//内存池申请空间
	T* New()
	{
		T* obj = nullptr;
		//若回收链表内有被回收回来的空间，则优先从里面分配空间给对象
		if (_freeList)
		{
			void* next = *(void**)_freeList;//取_freeList前4/8字节处所存的下一内存块地址
			obj = (T*)_freeList;//将空间给obj对象
			_freeList = next;//自由链表移动到下一个空间块（意味着前一个内存块已经被分割出去） 
		}
		//若自由链表内无回收空间
		else
		{
			if (_remainBytes < sizeof(T))//若内存池剩余空间小于即将申请空间的类型大小，且回收链表没有回收的空间，则申请空间
			{
				_remainBytes = 128 * 1024;//若空间申请成功，可用空间更新
				_memory = (char*)SystemAlloc(_remainBytes >> 13);//128KB 脱离malloc，使用系统接口申请空间（参数：页数 每页大小：8k）
				//_memory = (char*)malloc(_remainBytes);
				if (_memory == nullptr)//若空间开辟失败
				{
					throw std::bad_alloc();//抛异常
				}
			}
			obj = (T*)_memory;//返回对象指向内存池头指针
			//若对象的类型大小小于指针大小则无法构建自由链表结果，因为存不下地址
			//所以，当对象申请的空间大小小于指针大小时，就直接分配一个指针大小的空间
			size_t objSize = sizeof(T) < sizeof(void*) ? sizeof(void*) : sizeof(T);//预防char\short类型
			_memory += objSize;//内存池头指针向后走T字节大小的空间，将空间分割出去（但若T类型大小大于内存池剩余空间该如何解决？）
			_remainBytes -= objSize;//内存池剩余空间减去分割出去的空间大小
		}

		//模拟new形式为：当空间被开辟后，还需要进行构造，此时需要定位new
		new(obj)T;//定位new(进行构造）

		return obj;
	}

	//回收内存块（维护自由链表）
	void Delete(T* obj)
	{
		obj->~T();//显式调用析构,清理对象内部数据
		*(void**)obj = _freeList;//将obj的前4/8byte存如下一节点地址：_freeList
		//指针在不同平台的大小是不一样的，32位：4byte，64位：8byte，故对obj进行强转为二级指针，再进行解引用，就可以对obj的前4/8byte的空间进行“读或写”操作
		_freeList = obj;//_freeList头指针指向头插进来的obj
	}

private:
	char* _memory = nullptr;//指向内存池(char类型大小为一字节，方便后期内存分割（单位））
	size_t _remainBytes = 0;//内存池切分剩余空间
	void* _freeList = nullptr;//内存块回收链表头指针（要求：每个内存块在32位下不低于4byte，64位下不低于8byte）（头4/8字节连接每一个待回收内存块）
};

//测试用例结构体
struct TreeNode
{
	int _val;
	TreeNode* _left;
	TreeNode* _right;

	TreeNode()
		:_val(0)
		,_left(nullptr)
		,_right(nullptr)
	{}
};

//测试内存池性能
//void TestObjectPool()
//{
//	const size_t Rounds = 5;//构建对象的轮数
//	const size_t N = 1000000;//构建对象的次数
//
//	std::vector<TreeNode*> v1;//vector例子
//	v1.reserve(N);
//	//构建与析构对象Rounds轮(vector)
//	size_t begin_1 = clock();//记录运行开始的时间(vector用例开始时间记录）
//	for (size_t j = 0; j < Rounds; j++)
//	{
//		for (int i = 0; i < N; i++)
//		{
//			v1.push_back(new TreeNode);
//		}
//		for (int i = 0; i < N; i++)
//		{
//			delete v1[i];
//		}
//		v1.clear();
//	}
//	size_t end_1 = clock();//记录运行结束的时间(vector用例结束时间记录）
//
//	std::vector<TreeNode*> v2;
//	v2.reserve(N);
//	//构建与析构对象Rounds轮(内存池)
//	ObjectPool<TreeNode> TNPool;//构造内存池对象
//	size_t begin_2 = clock();//记录运行开始的时间(内存池用例开始时间记录）
//	for (size_t j = 0; j < Rounds; j++)
//	{
//		for (int i = 0; i < N; i++)
//		{
//			v2.push_back(TNPool.New());
//		}
//		for (int i = 0; i < N; i++)
//		{
//			TNPool.Delete(v2[i]);
//		}
//		v2.clear();
//	}
//	size_t end_2 = clock();//记录运行结束的时间(内存池用例结束时间记录）
//
//	cout << "new coat time: " << end_1 - begin_1 << std::endl;//new的时间消耗
//	cout << "object pool cost time: " << end_2 - begin_2 << std::endl;//内存池的时间消耗 
//}
