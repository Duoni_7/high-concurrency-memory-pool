#pragma once
#include"common.h"

//ThreadCache(第一层）管理线程内部256KB大小的内存结构
class ThreadCache
{
public:
	//申请内存对象
	void* Allocate(size_t size);//参数size为哈希桶对应的下标
	//释放内存对象
	void Deallocate(void* ptr, size_t size);//size同理
	//向中心内存获取内存对象
	void* FetchFromCentralCache(size_t index, size_t size);
	// threadcache层释放对象时，链表过长时，回收内存回到中心缓存
	void ListTooLong(FreeList& list, size_t size);
private:
	FreeList _freeLists[NFREELIST];//哈希桶-208个（存放自由链表的哈希桶） 
};

//实现无锁访问中心缓存 
static _declspec(thread) ThreadCache* pTLSThreadCache = nullptr;//使每个线程都拥有自己的TLS对象(哈希桶），互不干扰，只能使用自身的资源
