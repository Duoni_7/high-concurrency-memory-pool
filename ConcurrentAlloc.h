#pragma once

#include"common.h"
#include"threadCache.h"
#include"PageCache.h"
#include"ObjectPool.h"

//获取线程缓冲区资源（OS接口）(多个线程会访问此接口）
static void* ConcurrentAlloc(size_t size)
{
	if (size > MAX_BYTES)//如果线程申请的对象大小超过线程缓存区极限：256KB
	{
		size_t alignSize = SizeClass::RoundUp(size);//进行对象大小对齐
		size_t kpage = alignSize >> PAGE_SHIFT;// 对齐后的对象大小 / 8K == 所需的页数 “需要向page申请一个kpage大小的span”

		PageCache::GetInstance()->_pageMtx.lock();//加锁
		Span* span = PageCache::GetInstance()->NewSpan(kpage);//申请一个kpage大小的span
		span->_objSize = size;//记录span的大小
		PageCache::GetInstance()->_pageMtx.unlock();//加锁

		void* ptr = (void*)(span->_pageId << PAGE_SHIFT);//求该span的地址：页号*8K
		return ptr;
	}
	else//申请的对象大小没有超过缓冲区上限
	{
		if (pTLSThreadCache == nullptr)//若线程缓冲区无资源
		{
			static ObjectPool< ThreadCache> tcPool;//申请ThreadCache的地方比较少
			pTLSThreadCache = tcPool.New();//（申请一个存有自由链表的哈希桶（256KB））且将所有自由链表单元都初始化为nullptr
		}
		/*cout << std::this_thread::get_id() << " : " << pTLSThreadCache << std::endl;*/
		return pTLSThreadCache->Allocate(size);//若缓冲区有资源，直接由哈希桶分配内存块给对象
	}
}

//释放中心缓存资源
static void ConcurrentFree(void* ptr)//ptr是需要释放的资源 size最终会被转化为下标，挂在对应桶下标的自由链表中
{
	Span* span = PageCache::GetInstance()->MapObjectSpan(ptr);//获取span的地址
	size_t size = span->_objSize;//span的大小
	if (size > MAX_BYTES)//若待释放对象大于256字节
	{
		//会出现两种情况： 1.可能是向pagecache取的span?  2.也有可能是向堆空间申请的？(直接挂回到page层)
		PageCache::GetInstance()->_pageMtx.lock();//加锁
		PageCache::GetInstance()->ReleaseSpanToPageCache(span);//将span节点还给page层
		PageCache::GetInstance()->_pageMtx.unlock();//解锁
	}
	else
	{
		assert(pTLSThreadCache);//该线程的缓冲区不为空
		pTLSThreadCache->Deallocate(ptr, size);
	}
}