#define  _CRT_SECURE_NO_WARNINGS 1
#include"ObjectPool.h"
#include"ConcurrentAlloc.h"

void MultiThreadAlloc_1()
{
	std::vector<void*> v;
	for (int i = 0; i < 7; i++)
	{
		void* ptr = ConcurrentAlloc(6);
		v.push_back(ptr);
	}

	for (auto e : v)//释放五次
	{
		ConcurrentFree(e);
	}
}

void MultiThreadAlloc_2()
{
	std::vector<void*> v;
	for (int i = 0; i < 7; i++)
	{
		void* ptr = ConcurrentAlloc(6);
		v.push_back(ptr);
	}

	for (auto e : v)//释放五次
	{
		ConcurrentFree(e);
	}
}

void TestMultiThread()
{
	//创建两个线程
	std::thread t1(MultiThreadAlloc_1);
	std::thread t2(MultiThreadAlloc_2);
	t1.join();
	t2.join();
}

void TestConcurrentAlloc_1()
{
	void* p1 = ConcurrentAlloc(6);
	void* p2 = ConcurrentAlloc(8);
	void* p3 = ConcurrentAlloc(1);
	void* p4 = ConcurrentAlloc(7);
	void* p5 = ConcurrentAlloc(8);
	void* p6 = ConcurrentAlloc(8);
	void* p7 = ConcurrentAlloc(8);

	cout << p1 << std::endl;
	cout << p2 << std::endl;
	cout << p3 << std::endl;
	cout << p4 << std::endl;
	cout << p5 << std::endl;

	ConcurrentFree(p1);
	ConcurrentFree(p2);
	ConcurrentFree(p3);
	ConcurrentFree(p4);
	ConcurrentFree(p5);
	ConcurrentFree(p6);
	ConcurrentFree(p7);
}

void TestConcurrentAlloc_2()
{
	for (size_t i = 0; i < 1024; i++)
	{
		void* p1 = ConcurrentAlloc(6);
		cout << p1 << std::endl;
	}
	void* p2 = ConcurrentAlloc(6);
	cout << p2 << std::endl;
}

void TestConcurrentAlloc_3()
{
	for (size_t i = 0; i < 5; i++)
	{
		void* p1 = ConcurrentAlloc(6);
		cout << p1 << std::endl;
	}
	void* p2 = ConcurrentAlloc(6);
	cout << p2 << std::endl;
}

void BigAlloc()
{
	void* p1 = ConcurrentAlloc(257 * 1024);//超过线程私有缓冲区大小257K
	ConcurrentFree(p1);

	void* p2 = ConcurrentAlloc(129 * 8 * 1024);//超过page cache层大小 129页
	ConcurrentFree(p2);
}

//单元测试
//int main()
//{
//	BigAlloc();//大对象申请
	//TestMultiThread();//多线程申请释放测试
	//TestConcurrentAlloc_1();//常规释放逻辑测试
	//TestObjectPool();//定长内存池测试
	//TLSTest();
	//cout << sizeof PAGE_ID << std::endl;
//	return 0;
//}