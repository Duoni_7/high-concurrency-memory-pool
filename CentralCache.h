#pragma once

#include"common.h"//对齐规则/哈希桶映射原则

//中心缓存区全局只有一个（单例模式）设计
class CentralCache
{
public:

	//获取中心缓存对象
	static CentralCache* GetInstance()
	{
		return &_sInst;
	}

	//获取一个非空的Span
	Span* GetOneSpan(SpanList& list, size_t size);

	// 从中心缓存获取一定数量的对象给thread cache
	size_t FetchRangeObj(void*& start, void*& end, size_t batchNum, size_t size);

	// 将一定数量的对象释放到span跨度
	void ReleaseListToSpans(void* start, size_t size);

private:
	SpanList _spanLists[NFREELIST];//Span哈希桶大小为208个 与thread cache层哈希桶保持一致
private:

	CentralCache(){}
	//拷贝构造（不生成）
	CentralCache(const CentralCache&) = delete;


	static CentralCache _sInst;
};