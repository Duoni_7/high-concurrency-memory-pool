#include"CentralCache.h"
#include"PageCache.h"

CentralCache CentralCache::_sInst;//构造中心缓存对象(main调用前执行）

//获取一个非空的Span(向 page cache 层获取）
Span* CentralCache::GetOneSpan(SpanList& list, size_t size)
{
	Span* it = list.Begin();//获取第一个元素位置

	//遍历寻找桶位对应的span链表中的非空节点
	while (it != list.End())
	{
		if (it->_freeList != nullptr)//查看：若该span节点不为空
		{
			return it;//则返回该span节点，供thread cache分割
		}
		else
		{
			it = it->_next;
		}
	}

	//先把中心内存中加的锁先解锁，因为这样如果其他线程释放（带回）内存对象回来，不会阻塞
	list._mtx.unlock();//解锁：FetchRangeObj函数中的锁

	PageCache::GetInstance()->_pageMtx.lock();//加锁，因为多个进程可能同时要申请一个span
	//若执行到此，则说明该span链表都为空，则需要向page cache申请页
	Span* span = PageCache::GetInstance()->NewSpan(SizeClass::NumMovePage(size));//向pagecache层获取一个span页
	span->_isUse = true;//sapn正在被使用
	span->_objSize = size;//保存好申请的span的大小
	PageCache::GetInstance()->_pageMtx.unlock();//解锁

	//切割时不需要加锁，因为此时不对spanlist进行访问或操作
	char* start = (char*)(span->_pageId << PAGE_SHIFT);//计算出该span页的起始地址: (页编号 * （8*1024）=页起始位置)
	size_t bytes = span->_n << PAGE_SHIFT;//计算该span的总大小：页数 * 8 * 1024 == 总字节数大小 ->分配一页给中心缓存区，供线程缓存申请
	char* end = start + bytes;//计算页的结尾地址：开始位置指针移动bytes位（前闭后开）

	//将该大块内存进行细化切分（切割成span->_freelist中的一个个小内存块）  
	//一页=8K，会被切分成为1024个小内存块
	span->_freeList = start;//先切分出一块节点作为头节点，方便尾插
	start += size;//start向下一个位置移动
	void* tail = span->_freeList;//游标->此时start已经成为头节点
	int i = 1;
	while (start < end)
	{
		i++;
		//尾插，保证内存块的顺序性(用指针存地址模拟出来的链表结构）
		NextObj(tail) = start;//头节点的前4/8字节存入：下一个start的地址
		tail = NextObj(tail);//tail向后移
		start += size;//大块空间中的start继续后移
	}

	NextObj(tail) = nullptr;//置空尾节点

	list._mtx.lock();//恢复锁：这里要把切割好的span插入桶中
	list.PushFront(span);//将span页链接进spanlist中

	return span;  
}

// 从中心缓存获取一定数量的对象给thread cache(并返回实际获取数量）
size_t CentralCache::FetchRangeObj(void*& start, void*& end, size_t batchNum, size_t size)
{
	size_t index = SizeClass::Index(size);//找到中心缓存的对应桶下标，看看是否有Span空间
	_spanLists[index]._mtx.lock();//加锁

	Span* span = GetOneSpan(_spanLists[index], size);//向 page cache 申请一个span块(获取的是对应span桶的span链表头指针）
	assert(span);//确保这个span节点不为空
	assert(span->_freeList);//确保该span节点对象的自由链表不为空

	//从span中获取内存块，若内部资源不过batchNum，则有多少取多少
	start = span->_freeList;//start与end指向span节点的_freelist头指针
	end = start;
	size_t actualNum = 1;//记录获取到内存块的个数（定义为1，是因为span肯定不为空）
	size_t i = 0;
	while(i < batchNum-1 && NextObj(end) != nullptr)//end向后走batchNum-1步,并且end的下一个节点不能为空（防止batchNum大于_freelist的总节点数量，防止溢出）
	{
		end = NextObj(end);//end向后走
		++i;
		++actualNum;
	}

	span->_freeList = NextObj(end);//_freelist指向end的下一个
	NextObj(end) = nullptr;//end节点的next置为空，此时的start到end便是取到了对应的批量个节点
	span->_useCount += actualNum;//span页中的usecount是一个计数器，内存块被拿走多少就加多少，当usecount为0时，就说明span中的内存块都被还回了

	_spanLists[index]._mtx.unlock();//解开锁
	return actualNum;
}

// 将一定数量的对象(thread cache)释放到span跨度（size直接映射桶位）
void CentralCache::ReleaseListToSpans(void* start, size_t size)
{
	size_t index = SizeClass::Index(size);//算出对应spanlist的桶下标
	_spanLists[index]._mtx.lock();//即将对桶位进行操作，加上桶锁

	//小块内存被回收进对应span
	//找到了对应的桶位，那怎么找到是spanlist中的哪一个span节点呢？ 通过一组页号与span*的映射关系解决
	while (start)//start走到空停止
	{
		void* next = NextObj(start);//记录start的下一个位置
		Span* span = PageCache::GetInstance()->MapObjectSpan(start);//获取对应的span地址

		//头插
		NextObj(start) = span->_freeList;//start头插进span节点下_freelist之前
		span->_freeList = start;//头指针调整位置

		span->_useCount--;//内存块被归还，计数--
		if (span->_useCount == 0)//说明span内存块都被归还回来，而此时需要将该span归还给page cache
		{
			_spanLists[index].Erase(span);//删除span节点
			span->_freeList = nullptr;//小块内存全部被去除6，因为已经没有用。只要知道span的 起始与结束地址， 便可以看作是一块完整的span内存
			span->_next = nullptr;//置空
			span->_prev = nullptr;//置空

			//处理后，将span节点还给page cache层 (此处涉及锁问题）
			_spanLists[index]._mtx.unlock();//解桶锁：即将进入page cache归还span，及时解锁保证其他线程也可以进行以上内存块归还动作

			PageCache::GetInstance()->_pageMtx.lock();//上page cache的锁
			PageCache::GetInstance()->ReleaseSpanToPageCache(span);//归还span
			PageCache::GetInstance()->_pageMtx.unlock();//解page cache的锁


			_spanLists[index]._mtx.lock();//上锁，归还动作完毕
		}

		start = next;//start后移
	}
	_spanLists[index]._mtx.unlock();//解锁
}