#pragma once

#include"common.h"
#include"ObjectPool.h"
#include"PageMap.h"

//全局唯一：单例模式（饿汉）
//pagecache层（供central cache层申请内存）
class PageCache
{
public:

	//获取一个Page cache对象
	static PageCache* GetInstance()
	{
		return &_sInst;
	}

	//获取对象到span的映射
	Span* MapObjectSpan(void* obj);

	//释放span节点回到page cache，合并内部相邻的span节点成为更大的页(解决外部碎片的问题）
	void ReleaseSpanToPageCache(Span* span);

	//获取一个k页的span
	Span* NewSpan(size_t k);

	std::mutex _pageMtx;//锁

private:
	SpanList _spanLists[NPAGES];//Pagecache层哈希桶，内部以span页为单位（直接映射）
	ObjectPool<Span> _spanPool;//定义一个内存池对象

	//std::unordered_map<PAGE_ID, Span*> _idSpanMap;//页号与span指针地址的映射
	//std::map<PAGE_ID, Span*> _idSpanMap;//页号与span指针地址的映射
	TCMalloc_PageMap1<32 - PAGE_SHIFT> _idSpanMap;

	PageCache() {};
	PageCache(const PageCache&) = delete;//不生成拷贝构造

	static PageCache _sInst;
};